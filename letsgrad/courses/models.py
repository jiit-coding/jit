from django.db import models

class Course(models.Model):
    RATING_CHOICES=(
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    )
    LEVEL_CHOICES=(
        ('Beginner','Beginner'),
        ('Intermediate','Intermediate'),
        ('Expert','Expert')
    )
    course_title=models.CharField(max_length=200)
    course_description=models.TextField(max_length=300,default="")
    trainer_name=models.CharField(max_length=200)
    trainer_profile=models.CharField(max_length=300)
    career_link=models.CharField(max_length=200)
    target_audience=models.CharField(max_length=200)
    price = models.DecimalField(max_digits=10,decimal_places=2,default="0.0")
    skills_covered=models.TextField(max_length=200,default=None,)
    exercise_files=models.CharField(blank=True,null=True,max_length=500)
    workbook=models.CharField(blank=True,null=True,max_length=500)
    certificate=models.CharField(max_length=300)
    rating=models.CharField(choices=RATING_CHOICES,max_length=2,default="1")
    no_of_student_enrolled=models.IntegerField()
    no_of_views=models.IntegerField()
    level=models.CharField(max_length=13,choices=LEVEL_CHOICES,default="Beginner")
    duration_of_course=models.CharField(max_length=20)
    Content=models.CharField(max_length=300)

class Video(models.Model):
    video=models.CharField(max_length=200)
    duration=models.CharField(max_length=200)
    course_id=models.ForeignKey(Course,on_delete=models.CASCADE)


class Assignment(models.Model):
    assignment_file=models.CharField(max_length=500)
    quizzes=models.CharField(max_length=500)
    course_id = models.ForeignKey(Course,on_delete=models.CASCADE)

