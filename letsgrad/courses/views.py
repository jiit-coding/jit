from django.shortcuts import render
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
    )
from courses.serializers import CourseSerializer,VideoSerializers,AssignmentSerializers
from courses.models import Course,Video,Assignment


class CourseListView(ListAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseCreateView(CreateAPIView):
    # queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseDetailView(RetrieveAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    lookup_field = 'id'


class CourseUpdateView(RetrieveUpdateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    lookup_field = 'id'


class CourseDeleteView(DestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    lookup_field = 'id'

class VideoListView(ListAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializers

class VideoCreateView(CreateAPIView):
    serializer_class = VideoSerializers

class VideoDetailView(RetrieveAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializers
    lookup_field = 'id'

class VideoDeleteView(DestroyAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializers
    lookup_field = 'id'

class VideoUpdate(RetrieveUpdateAPIView):
    queryset = Video.objects.all()
    serializer_class = VideoSerializers
    lookup_field = 'id'

class AssignmentListView(ListAPIView):
    queryset = Assignment.objects.all()
    serializer_class = AssignmentSerializers


class AssignmentCreateView(CreateAPIView):
    serializer_class =AssignmentSerializers

class AssignmentDetailView(RetrieveAPIView):
    queryset = Assignment.objects.all()
    serializer_class =AssignmentSerializers
    lookup_field = 'id'


class AssignmentUpdateView(RetrieveUpdateAPIView):
    queryset = Assignment.objects.all()
    serializer_class = AssignmentSerializers
    lookup_field = 'id'


class AssignmentDeleteView(DestroyAPIView):
    queryset = Assignment.objects.all()
    serializer_class = AssignmentSerializers
    lookup_field = 'id'











